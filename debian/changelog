neutron-taas (10.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Add python3-webtest as build-depends.
  * Rebased remove-auto_spec.patch.

 -- Thomas Goirand <zigo@debian.org>  Thu, 06 Oct 2022 10:57:34 +0200

neutron-taas (9.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Mar 2022 22:47:13 +0200

neutron-taas (9.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 15:46:06 +0100

neutron-taas (9.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 14 Mar 2022 17:03:55 +0100

neutron-taas (8.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Debhelper compat level 11.
  * Fixed (build-)depends for this release, and remove versions when satisfied
    in Bullseye.
  * Add remove-auto_spec.patch (Closes: #1002427).

 -- Thomas Goirand <zigo@debian.org>  Thu, 23 Dec 2021 14:46:43 +0100

neutron-taas (7.0.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Run wrap-and-sort -bastk.
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Replaced upstream VCS URLs, as it's not mirrored in Github anymore.
  * New upstream release.
  * Fixed (build-)depends for this release. (Closes: #973116).

 -- Thomas Goirand <zigo@debian.org>  Mon, 02 Nov 2020 10:20:07 +0100

neutron-taas (5.0.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 20 Jul 2019 17:35:20 +0200

neutron-taas (3.0.0+2018.08.05.git.84846d52fd-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release:
    - Fix FTBFS with latest Neutron (Closes: #909371).
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 24 Sep 2018 10:10:34 +0200

neutron-taas (3.0.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Switched to Python 3 (Closes: #892884).
  * Updated debian/copyright for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Mar 2018 23:52:27 +0100

neutron-taas (2.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 17 Nov 2017 15:01:17 +0000

neutron-taas (2.0.0-1) experimental; urgency=medium

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release:
    - Fixes unit tests (Closes: #877242).
  * Fixed (build-)depends for this release.
  * neutron-taas-openvswitch-agent depends on lsb-base.
  * Standards-Version is now 4.1.1.
  * Added myself as uploader.
  * Do not define OSLO_PACKAGE_VERSION, defined in openstack-pkg-tools.
  * Using debhelper 10.
  * python-neutron-taas in section python.

 -- Thomas Goirand <zigo@debian.org>  Tue, 31 Oct 2017 12:15:23 +0100

neutron-taas (0.0.0+git20160808.c612a729-1) experimental; urgency=medium

  * Initial release (Closes: #833736).

 -- James Page <james.page@ubuntu.com>  Tue, 09 Aug 2016 14:18:29 +0100
