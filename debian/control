Source: neutron-taas
Section: net
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 James Page <james.page@ubuntu.com>,
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-babel,
 python3-coverage,
 python3-hacking,
 python3-mock,
 python3-neutron,
 python3-neutron-lib (>= 2.11.0),
 python3-oslosphinx,
 python3-oslotest,
 python3-psycopg2,
 python3-pymysql,
 python3-stestr,
 python3-requests-mock,
 python3-testresources,
 python3-testscenarios,
 python3-testtools,
 python3-webtest,
 subunit,
 testrepository,
Standards-Version: 4.4.1
Homepage: https://opendev.org/x/tap-as-a-service
Vcs-Browser: https://salsa.debian.org/openstack-team/services/neutron-taas
Vcs-Git: https://salsa.debian.org/openstack-team/services/neutron-taas.git

Package: neutron-taas-openvswitch-agent
Architecture: all
Depends:
 neutron-common (>= 2:16.0.0),
 python3-neutron-taas (= ${binary:Version}),
 ${misc:Depends},
 ${ostack-lsb-base},
 ${python3:Depends},
Description: OpenStack virtual network service - Tap-as-a-Service agent
 Neutron provides an API to dynamically request and configure virtual networks.
 These networks connect "interfaces" from other OpenStack services (such as
 vNICs from Nova VMs). The Neutron API supports extensions to provide advanced
 network capabilities, including QoS, ACLs, and network monitoring.
 .
 Tap-as-a-Service (TaaS) is an extension to the OpenStack network service
 (Neutron). It provides remote port mirroring capability for tenant virtual
 networks.
 .
 Port mirroring involves sending a copy of packets entering and/or leaving one
 port to another port, which is usually different from the original
 destinations of the packets being mirrored.
 .
 This service has been primarily designed to help tenants (or the cloud
 administrator) debug complex virtual networks and gain visibility into their
 VMs, by monitoring the network traffic associated with them. TaaS honors
 tenant boundaries and its mirror sessions are capable of spanning across
 multiple compute and network nodes. It serves as an essential infrastructure
 component that can be utilized for supplying data to a variety of network
 analytics and security applications (e.g. IDS).
 .
 This package provides the Tap-as-a-Service agent for Open vSwitch.

Package: python3-neutron-taas
Architecture: all
Section: python
Depends:
 python3-babel,
 python3-neutron,
 python3-neutron-lib (>= 2.11.0),
 python3-pbr,
 ${misc:Depends},
 ${python3:Depends},
Enhances:
 python3-neutronclient,
Breaks:
 python-neutron-taas,
Replaces:
 python-neutron-taas,
Description: OpenStack virtual network service - Tap-as-a-Service extension
 Neutron provides an API to dynamically request and configure virtual networks.
 These networks connect "interfaces" from other OpenStack services (such as
 vNICs from Nova VMs). The Neutron API supports extensions to provide advanced
 network capabilities, including QoS, ACLs, and network monitoring.
 .
 Tap-as-a-Service (TaaS) is an extension to the OpenStack network service
 (Neutron). It provides remote port mirroring capability for tenant virtual
 networks.
 .
 Port mirroring involves sending a copy of packets entering and/or leaving one
 port to another port, which is usually different from the original
 destinations of the packets being mirrored.
 .
 This service has been primarily designed to help tenants (or the cloud
 administrator) debug complex virtual networks and gain visibility into their
 VMs, by monitoring the network traffic associated with them. TaaS honors
 tenant boundaries and its mirror sessions are capable of spanning across
 multiple compute and network nodes. It serves as an essential infrastructure
 component that can be utilized for supplying data to a variety of network
 analytics and security applications (e.g. IDS).
 .
 This package provides the Python files for the Tap-as-a-Service (TaaS)
 extension.
